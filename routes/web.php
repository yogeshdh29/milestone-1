<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('about', 'about')->middleware('test');
Route::get('customers', 'CustomersController@index')->name('customers.index');
Route::get('customersx', 'CustomersController@indexx');

Route::get('customers/create', 'CustomersController@create');
Route::post('customers', 'CustomersController@store');


Route::get('customers/{customer}', 'CustomersController@show')->middleware('can:view,customer');

Route::get('customers/{customer}/edit', 'CustomersController@edit');
Route::patch('customers/{customer}', 'CustomersController@update')->name('customers.update');


Route::delete('customers/{customer}', 'CustomersController@destroy');

Route::get('contact', 'ContactFormController@create');
Route::post('contact', 'ContactFormController@store')->name('contact.store');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

