@extends('layouts.appx')


@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <h1>Customer List</h1>      
    </div>
</div>

@can('create', \App\Customer::class)
<div class="row">
    <div class="col-12">
        <p><a href="customers/create">Add New Customer</a></p>
    </div>
</div>
@endcan
    <div class="row">
        <div class="col-12">
            <table class="table" id="datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Company</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($customers as $customer)
                    <tr>
                        <td>{{ $customer->id }}</td>
                        <td>
                            @can('view', $customer)
                            <a href="/customers/{{ $customer->id }}">
                                {{ $customer->name }}
                            </a>
                            @endcan

                            @cannot('view', $customer)
                                {{ $customer->name }}
                            @endcan                                
                        </td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->active }}</td>
                        <td>{{ $customer->company->name }}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        });
    </script>
@endsection
